<?php

	class OptiWorldCurrencyWidget extends WP_Widget {

		function __construct() {
			$widget_ops = array('classname' => 'OptiWorldCurrencyWidget', 'description' => 'Shows the currency selection box if needed' );
			$this->WP_Widget('OptiWorldCurrencyWidget', 'Opti World Currency', $widget_ops);
		}

		function OptiWorldCurrencyWidget() {
			self::__construct();
		}

		function form($instance) {
		}

		function update($new_instance, $old_instance) {
		}

		function widget($args, $instance) {
			global $post;
      // Keep this 'worldcurrency' as is so that the same shortcode is used.
			if (strpos($post->post_content, 'worldcurrency') !== false)
				echo dt_owc_getCurrencySelectionBox();
		}

	}

	add_action( 'widgets_init', create_function('', 'return register_widget("OptiWorldCurrencyWidget");') );
