<?php

	/**
	 * Currency lists thanks to Stephen Cronin (http://www.scratch99.com/)
	 */

	// Create the currency list array
	$dt_owc_currencylist = array();
	$dt_owc_currencylist['USD'] = array('name'=>'U.S. Dollar (USD)', 'symbol'=>'$');
	$dt_owc_currencylist['GBP'] = array('name'=>'British Pound (GBP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['EUR'] = array('name'=>'Euro (EUR)', 'symbol'=>'&#8364;');
	$dt_owc_currencylist['AUD'] = array('name'=>'Australian Dollar (AUD)', 'symbol'=>'$');
	$dt_owc_currencylist['CAD'] = array('name'=>'Canadian Dollar (CAD)', 'symbol'=>'$');
	$dt_owc_currencylist['NZD'] = array('name'=>'New Zealand Dollar (NZD)', 'symbol'=>'$');
	$dt_owc_currencylist['CNY'] = array('name'=>'Chinese Yuan (CNY)', 'symbol'=>'&#20803;');
	$dt_owc_currencylist['JPY'] = array('name'=>'Japanese Yen (JPY)', 'symbol'=>'&#165;');
	$dt_owc_currencylist['RUB'] = array('name'=>'Russian Rouble (RUB)', 'symbol'=>'&#1088;&#1091;&#1073;');
	$dt_owc_currencylist['---'] = array('name'=>'-------------------', 'symbol'=>'---');
	$dt_owc_currencylist['ARS'] = array('name'=>'Argentine Peso (ARS)', 'symbol'=>'$');
	$dt_owc_currencylist['AWG'] = array('name'=>'Aruba Florin (AWG)', 'symbol'=>'&#402;');
	$dt_owc_currencylist['BBD'] = array('name'=>'Barbados Dollar (BBD)', 'symbol'=>'$');
	$dt_owc_currencylist['BDT'] = array('name'=>'Bangladesh Taka (BDT)', 'symbol'=>'');
	$dt_owc_currencylist['BGN'] = array('name'=>'Bulgarian Lev (BGN)', 'symbol'=>'&#1083;&#1074;');
	$dt_owc_currencylist['BHD'] = array('name'=>'Bahraini Dinar (BHD)', 'symbol'=>'&#1576;.&#1583;');
	$dt_owc_currencylist['BIF'] = array('name'=>'Burundi Franc (BIF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['BMD'] = array('name'=>'Bermuda Dollar (BMD)', 'symbol'=>'$');
	$dt_owc_currencylist['BND'] = array('name'=>'Brunei Dollar (BND)', 'symbol'=>'$');
	$dt_owc_currencylist['BOB'] = array('name'=>'Bolivian Boliviano (BOB)', 'symbol'=>'$b');
	$dt_owc_currencylist['BRL'] = array('name'=>'Brazilian Real (BRL)', 'symbol'=>'R$');
	$dt_owc_currencylist['BSD'] = array('name'=>'Bahamian Dollar (BSD)', 'symbol'=>'$');
	$dt_owc_currencylist['BTN'] = array('name'=>'Bhutan Ngultrum (BTN)', 'symbol'=>'');
	$dt_owc_currencylist['BWP'] = array('name'=>'Botswana Pula (BWP)', 'symbol'=>'P');
	$dt_owc_currencylist['BYR'] = array('name'=>'Belarus Ruble (BYR)', 'symbol'=>'p.');
	$dt_owc_currencylist['BZD'] = array('name'=>'Belize Dollar (BZD)', 'symbol'=>'$');
	$dt_owc_currencylist['CHF'] = array('name'=>'Swiss Franc (CHF)', 'symbol'=>'CHF');
	$dt_owc_currencylist['CLP'] = array('name'=>'Chilean Peso (CLP)', 'symbol'=>'$');
	$dt_owc_currencylist['COP'] = array('name'=>'Colombian Peso (COP)', 'symbol'=>'$');
	$dt_owc_currencylist['CRC'] = array('name'=>'Costa Rica Colon (CRC)', 'symbol'=>'&#8353;');
	$dt_owc_currencylist['CUP'] = array('name'=>'Cuban Peso (CUP)', 'symbol'=>'&#8369;');
	$dt_owc_currencylist['CYP'] = array('name'=>'Cyprus Pound (CYP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['CZK'] = array('name'=>'Czech Koruna (CZK)', 'symbol'=>'K&#269;');
	$dt_owc_currencylist['DJF'] = array('name'=>'Dijibouti Franc (DJF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['DKK'] = array('name'=>'Danish Krone (DKK)', 'symbol'=>'kr');
	$dt_owc_currencylist['DOP'] = array('name'=>'Dominican Peso (DOP)', 'symbol'=>'RD$');
	$dt_owc_currencylist['DZD'] = array('name'=>'Algerian Dinar (DZD)', 'symbol'=>'&#1583;.&#1580;');
	$dt_owc_currencylist['ECS'] = array('name'=>'Ecuador Sucre (ECS)', 'symbol'=>'');
	$dt_owc_currencylist['EEK'] = array('name'=>'Estonian Kroon (EEK)', 'symbol'=>'kr');
	$dt_owc_currencylist['EGP'] = array('name'=>'Egyptian Pound (EGP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['SVC'] = array('name'=>'El Salvador Colon (SVC)', 'symbol'=>'$');
	$dt_owc_currencylist['ERN'] = array('name'=>'Eritrea Nakfa (ERN)', 'symbol'=>'Nfk');
	$dt_owc_currencylist['ETB'] = array('name'=>'Ethiopian Birr (ETB)', 'symbol'=>'');
	$dt_owc_currencylist['FKP'] = array('name'=>'Falkland Islands Pound (FKP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['FJD'] = array('name'=>'Fijian dollar (FJD)', 'symbol'=>'F$');
	$dt_owc_currencylist['GHC'] = array('name'=>'Ghanian Cedi (GHC)', 'symbol'=>'&#162;');
	$dt_owc_currencylist['GIP'] = array('name'=>'Gibraltar Pound (GIP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['GMD'] = array('name'=>'Gambian Dalasi (GMD)', 'symbol'=>'D');
	$dt_owc_currencylist['GNF'] = array('name'=>'Guinea Franc (GNF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['GTQ'] = array('name'=>'Guatemala Quetzal (GTQ)', 'symbol'=>'Q');
	$dt_owc_currencylist['HKD'] = array('name'=>'Hong Kong Dollar (HKD)', 'symbol'=>'HK$');
	$dt_owc_currencylist['HNL'] = array('name'=>'Honduras Lempira (HNL)', 'symbol'=>'L');
	$dt_owc_currencylist['HRK'] = array('name'=>'Croatian Kuna (HRK)', 'symbol'=>'kn');
	$dt_owc_currencylist['HTG'] = array('name'=>'Haiti Gourde (HTG)', 'symbol'=>'G');
	$dt_owc_currencylist['HUF'] = array('name'=>'Hungarian Forint (HUF)', 'symbol'=>'Ft');
	$dt_owc_currencylist['IDR'] = array('name'=>'Indonesian Rupiah (IDR)', 'symbol'=>'Rp');
	$dt_owc_currencylist['ILS'] = array('name'=>'Israeli Shekel (ILS)', 'symbol'=>'&#8362;');
	$dt_owc_currencylist['INR'] = array('name'=>'Indian Rupee (INR)', 'symbol'=>'&#8360;');
	$dt_owc_currencylist['IRR'] = array('name'=>'Iran Rial (IRR)', 'symbol'=>'&#65020;');
	$dt_owc_currencylist['ISK'] = array('name'=>'Iceland Krona (ISK)', 'symbol'=>'kr');
	$dt_owc_currencylist['JMD'] = array('name'=>'Jamaican Dollar (JMD)', 'symbol'=>'J$');
	$dt_owc_currencylist['JOD'] = array('name'=>'Jordanian Dinar (JOD)', 'symbol'=>'&#1583;.&#1575;');
	$dt_owc_currencylist['KES'] = array('name'=>'Kenyan Shilling (KES)', 'symbol'=>'Sh');
	$dt_owc_currencylist['KHR'] = array('name'=>'Cambodia Riel (KHR)', 'symbol'=>'&#6107;');
	$dt_owc_currencylist['KMF'] = array('name'=>'Comoros Franc (KMF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['KRW'] = array('name'=>'Korean Won (KRW)', 'symbol'=>'&#8361;');
	$dt_owc_currencylist['KWD'] = array('name'=>'Kuwaiti Dinar (KWD)', 'symbol'=>'&#1583;.&#1603;');
	$dt_owc_currencylist['KYD'] = array('name'=>'Cayman Islands Dollar (KYD)', 'symbol'=>'$');
	$dt_owc_currencylist['KZT'] = array('name'=>'Kazakhstan Tenge (KZT)', 'symbol'=>'&#1083;&#1074;');
	$dt_owc_currencylist['LAK'] = array('name'=>'Lao Kip (LAK)', 'symbol'=>'&#8365;');
	$dt_owc_currencylist['LBP'] = array('name'=>'Lebanese Pound (LBP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['LSL'] = array('name'=>'Lesotho Loti (LSL)', 'symbol'=>'L');
	$dt_owc_currencylist['LTL'] = array('name'=>'Lithuanian Lita (LTL)', 'symbol'=>'Lt');
	$dt_owc_currencylist['LVL'] = array('name'=>'Latvian Lat (LVL)', 'symbol'=>'Ls');
	$dt_owc_currencylist['LYD'] = array('name'=>'Libyan Dinar (LYD)', 'symbol'=>'&#1604;.&#1583;');
	$dt_owc_currencylist['MAD'] = array('name'=>'Moroccan Dirham (MAD)', 'symbol'=>'&#1583;.&#1605;.');
	$dt_owc_currencylist['MDL'] = array('name'=>'Moldovan Leu (MDL)', 'symbol'=>'L');
	$dt_owc_currencylist['MGF'] = array('name'=>'Malagasy Franc (MGF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['MKD'] = array('name'=>'Macedonian Denar (MKD)', 'symbol'=>'&#1076;&#1077;&#1085;');
	$dt_owc_currencylist['MNT'] = array('name'=>'Mongolian Tugrik (MNT)', 'symbol'=>'&#8366;');
	$dt_owc_currencylist['MOP'] = array('name'=>'Macau Pataca (MOP)', 'symbol'=>'P');
	$dt_owc_currencylist['MRO'] = array('name'=>'Mauritania Ougulya (MRO)', 'symbol'=>'UM');
	$dt_owc_currencylist['MTL'] = array('name'=>'Maltese Lira (MTL)', 'symbol'=>'Lm');
	$dt_owc_currencylist['MUR'] = array('name'=>'Mauritius Rupee (MUR)', 'symbol'=>'&#8360;');
	$dt_owc_currencylist['MVR'] = array('name'=>'Maldives Rufiyaa (MVR)', 'symbol'=>'&#1923;.');
	$dt_owc_currencylist['MWK'] = array('name'=>'Malawi Kwacha (MWK)', 'symbol'=>'MK');
	$dt_owc_currencylist['MXN'] = array('name'=>'Mexican Peso (MXN)', 'symbol'=>'$');
	$dt_owc_currencylist['MYR'] = array('name'=>'Malaysian Ringgit (MYR)', 'symbol'=>'RM');
	$dt_owc_currencylist['MZN'] = array('name'=>'Mozambique Metical (MZM)', 'symbol'=>'MTn');
	$dt_owc_currencylist['NAD'] = array('name'=>'Namibian Dollar (NAD)', 'symbol'=>'$');
	$dt_owc_currencylist['ANG'] = array('name'=>'Neth Antilles Guilder (ANG)', 'symbol'=>'&#402;');
	$dt_owc_currencylist['NGN'] = array('name'=>'Nigerian Naira (NGN)', 'symbol'=>'&#8358;');
	$dt_owc_currencylist['NIO'] = array('name'=>'Nicaragua Cordoba (NIO)', 'symbol'=>'C$');
	$dt_owc_currencylist['NOK'] = array('name'=>'Norwegian Krone (NOK)', 'symbol'=>'kr');
	$dt_owc_currencylist['NPR'] = array('name'=>'Nepalese Rupee (NPR)', 'symbol'=>'&#8360;');
	$dt_owc_currencylist['OMR'] = array('name'=>'Omani Rial (OMR)', 'symbol'=>'&#1585;.&#1593;.');
	$dt_owc_currencylist['PAB'] = array('name'=>'Panama Balboa (PAB)', 'symbol'=>'B/.');
	$dt_owc_currencylist['PEN'] = array('name'=>'Peruvian Nuevo Sol (PEN)', 'symbol'=>'S/.');
	$dt_owc_currencylist['PGK'] = array('name'=>'Papua New Guinea Kina (PGK)', 'symbol'=>'K');
	$dt_owc_currencylist['PHP'] = array('name'=>'Philippine Peso (PHP)', 'symbol'=>'Php');
	$dt_owc_currencylist['PKR'] = array('name'=>'Pakistani Rupee (PKR)', 'symbol'=>'&#8360;');
	$dt_owc_currencylist['PLN'] = array('name'=>'Polish Zloty (PLN)', 'symbol'=>'z&#322;');
	$dt_owc_currencylist['PYG'] = array('name'=>'Paraguayan Guarani (PYG)', 'symbol'=>'Gs');
	$dt_owc_currencylist['QAR'] = array('name'=>'Qatar Rial (QAR)', 'symbol'=>'&#1585;.&#1602;');
	$dt_owc_currencylist['RON'] = array('name'=>'Romanian New Leu (RON)', 'symbol'=>'lei');
	$dt_owc_currencylist['RWF'] = array('name'=>'Rwanda Franc (RWF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['SAR'] = array('name'=>'Saudi Arabian Riyal (SAR)', 'symbol'=>'&#1585;.&#1587;');
	$dt_owc_currencylist['SCR'] = array('name'=>'Seychelles Rupee (SCR)', 'symbol'=>'&#8360;');
	$dt_owc_currencylist['SDG'] = array('name'=>'Sudanese Dinar (SDD)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['SEK'] = array('name'=>'Swedish Krona (SEK)', 'symbol'=>'kr');
	$dt_owc_currencylist['SGD'] = array('name'=>'Singapore Dollar (SGD)', 'symbol'=>'$');
	$dt_owc_currencylist['SHP'] = array('name'=>'St Helena Pound (SHP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['SKK'] = array('name'=>'Slovak Koruna (SKK)', 'symbol'=>'SIT');
	$dt_owc_currencylist['SLL'] = array('name'=>'Sierra Leone Leone (SLL)', 'symbol'=>'Le');
	$dt_owc_currencylist['SOS'] = array('name'=>'Somali Shilling (SOS)', 'symbol'=>'S');
	$dt_owc_currencylist['LKR'] = array('name'=>'Sri Lanka Rupee (LKR)', 'symbol'=>'&#8360;');
	$dt_owc_currencylist['SRD'] = array('name'=>'Surinam Guilder (SRG)', 'symbol'=>'$');
	$dt_owc_currencylist['STD'] = array('name'=>'Sao Tome Dobra (STD)', 'symbol'=>'Db');
	$dt_owc_currencylist['SYP'] = array('name'=>'Syrian Pound (SYP)', 'symbol'=>'&#163;');
	$dt_owc_currencylist['SZL'] = array('name'=>'Swaziland Lilageni (SZL)', 'symbol'=>'L');
	$dt_owc_currencylist['THB'] = array('name'=>'Thai Baht (THB)', 'symbol'=>'&#3647;');
	$dt_owc_currencylist['TND'] = array('name'=>'Tunisian Dinar (TND)', 'symbol'=>'&#1583;.&#1578;');
	$dt_owc_currencylist['TOP'] = array('name'=>'Tonga Paanga (TOP)', 'symbol'=>'T$');
	$dt_owc_currencylist['TRY'] = array('name'=>'New Turkish Lira (TRY)', 'symbol'=>'YTL');
	$dt_owc_currencylist['TTD'] = array('name'=>'Trinidad&Tobago Dollar (TTD)', 'symbol'=>'TT$');
	$dt_owc_currencylist['TWD'] = array('name'=>'Taiwan Dollar (TWD)', 'symbol'=>'$');
	$dt_owc_currencylist['TZS'] = array('name'=>'Tanzanian Shilling (TZS)', 'symbol'=>'Sh');
	$dt_owc_currencylist['UAH'] = array('name'=>'Ukraine Hryvnia (UAH)', 'symbol'=>'&#8372;');
	$dt_owc_currencylist['UGX'] = array('name'=>'Ugandan Shilling (UGX)', 'symbol'=>'Sh');
	$dt_owc_currencylist['UYU'] = array('name'=>'Uruguayan New Peso (UYU)', 'symbol'=>'$U');
	$dt_owc_currencylist['VEB'] = array('name'=>'Venezuelan Bolivar (VEB)', 'symbol'=>'Bs');
	$dt_owc_currencylist['VND'] = array('name'=>'Vietnam Dong (VND)', 'symbol'=>'&#8363;');
	$dt_owc_currencylist['VUV'] = array('name'=>'Vanuatu Vatu (VUV)', 'symbol'=>'Vt');
	$dt_owc_currencylist['WST'] = array('name'=>'Samoa Tala (WST)', 'symbol'=>'T');
	$dt_owc_currencylist['XAF'] = array('name'=>'CFA Franc (BEAC) (XAF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['XCD'] = array('name'=>'East Caribbean Dollar (XCD)', 'symbol'=>'$');
	$dt_owc_currencylist['XOF'] = array('name'=>'CFA Franc (BCEAO) (XOF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['XPF'] = array('name'=>'Pacific Franc (XPF)', 'symbol'=>'Fr');
	$dt_owc_currencylist['AED'] = array('name'=>'UAE Dirham (AED)', 'symbol'=>'&#1583;.&#1573;');
	$dt_owc_currencylist['YER'] = array('name'=>'Yemen Riyal (YER)', 'symbol'=>'&#65020;');
	$dt_owc_currencylist['ZAR'] = array('name'=>'South African Rand (ZAR)', 'symbol'=>'R');
	$dt_owc_currencylist['ZMK'] = array('name'=>'Zambian Kwacha (ZMK)', 'symbol'=>'Zk');
	$dt_owc_currencylist['ZWD'] = array('name'=>'Zimbabwe Dollar (ZWD)', 'symbol'=>'Z$');

	// Create the currency location list array
	$dt_owc_locationlist = array(
		'AF' => 'AFA',
		'AL' => 'ALL',
		'DZ' => 'DZD',
		'AS' => 'USD',
		'AD' => 'EUR',
		'AO' => 'AON',
		'AI' => 'XCD',
		'AQ' => 'NOK',
		'AG' => 'XCD',
		'AR' => 'ARS',
		'AM' => 'AMD',
		'AW' => 'AWG',
		'AU' => 'AUD',
		'AT' => 'EUR',
		'AZ' => 'AZM',
		'BS' => 'BSD',
		'BH' => 'BHD',
		'BD' => 'BDT',
		'BB' => 'BBD',
		'BY' => 'BYR',
		'BE' => 'EUR',
		'BZ' => 'BZD',
		'BJ' => 'XAF',
		'BM' => 'BMD',
		'BT' => 'BTN',
		'BO' => 'BOB',
		'BA' => 'BAM',
		'BW' => 'BWP',
		'BV' => 'NOK',
		'BR' => 'BRR',
		'IO' => 'GBP',
		'BN' => 'BND',
		'BG' => 'BGN',
		'BF' => 'XAF',
		'BI' => 'BIF',
		'KH' => 'KHR',
		'CM' => 'XAF',
		'CA' => 'CAD',
		'CV' => 'CVE',
		'KY' => 'KYD',
		'CF' => 'XAF',
		'TD' => 'XAF',
		'CL' => 'CLF',
		'CN' => 'CNY',
		'CX' => 'AUD',
		'CC' => 'AUD',
		'CO' => 'COP',
		'KM' => 'KMF',
		'CG' => 'XAF',
		'CK' => 'NZD',
		'CR' => 'CRC',
		'CI' => 'XAF',
		'HR' => 'HRK',
		'CU' => 'CUP',
		'CY' => 'CYP',
		'CZ' => 'CZK',
		'DK' => 'DKK',
		'DJ' => 'DJF',
		'DM' => 'XCD',
		'DO' => 'DOP',
		'EC' => 'USD',
		'EG' => 'EGP',
		'SV' => 'USD',
		'GQ' => 'XAF',
		'ER' => 'ERN',
		'EE' => 'EEK',
		'ET' => 'ETB',
		'FK' => 'FKP',
		'FO' => 'DKK',
		'FJ' => 'FJD',
		'FI' => 'EUR',
		'FR' => 'EUR',
		'GF' => 'EUR',
		'PF' => 'XPF',
		'TF' => 'EUR',
		'GA' => 'XAF',
		'GM' => 'GMD',
		'GE' => 'GEL',
		'DE' => 'EUR',
		'GH' => 'GHC',
		'GI' => 'GIP',
		'GR' => 'EUR',
		'GL' => 'DKK',
		'GD' => 'XCD',
		'GP' => 'EUR',
		'GU' => 'USD',
		'GT' => 'GTQ',
		'GG' => 'GBP',
		'GN' => 'GNS',
		'GW' => 'GWP',
		'GY' => 'GYD',
		'HT' => 'HTG',
		'HM' => 'AUD',
		'VA' => 'EUR',
		'HN' => 'HNL',
		'HK' => 'HKD',
		'HU' => 'HUF',
		'IS' => 'ISK',
		'IN' => 'INR',
		'ID' => 'IDR',
		'IR' => 'IRR',
		'IQ' => 'IQD',
		'IE' => 'EUR',
		'IM' => 'GBP',
		'IL' => 'ILS',
		'IT' => 'EUR',
		'JM' => 'JMD',
		'JP' => 'JPY',
		'JE' => 'GBP',
		'JO' => 'JOD',
		'KZ' => 'KZT',
		'KE' => 'KES',
		'KI' => 'AUD',
		'KR' => 'KRW',
		'KW' => 'KWD',
		'KG' => 'KGS',
		'LA' => 'LAK',
		'LV' => 'LVL',
		'LB' => 'LBP',
		'LS' => 'LSL',
		'LR' => 'LRD',
		'LY' => 'LYD',
		'LI' => 'CHF',
		'LT' => 'LTL',
		'LU' => 'EUR',
		'MO' => 'MOP',
		'MK' => 'MKD',
		'MG' => 'MGF',
		'MW' => 'MWK',
		'MY' => 'MYR',
		'MV' => 'MVR',
		'ML' => 'XAF',
		'MT' => 'MTL',
		'MH' => 'USD',
		'MQ' => 'EUR',
		'MR' => 'MRO',
		'MU' => 'MUR',
		'YT' => 'EUR',
		'MX' => 'MXN',
		'MC' => 'EUR',
		'MN' => 'MNT',
		'ME' => 'EUR',
		'MS' => 'XCD',
		'MA' => 'MAD',
		'MZ' => 'MZM',
		'MM' => 'MMK',
		'NA' => 'NAD',
		'NR' => 'AUD',
		'NP' => 'NPR',
		'NL' => 'EUR',
		'AN' => 'ANG',
		'NC' => 'XPF',
		'NZ' => 'NZD',
		'NI' => 'NIC',
		'NE' => 'XOF',
		'NG' => 'NGN',
		'NU' => 'NZD',
		'NF' => 'AUD',
		'MP' => 'USD',
		'NO' => 'NOK',
		'OM' => 'OMR',
		'PK' => 'PKR',
		'PW' => 'USD',
		'PA' => 'PAB',
		'PG' => 'PGK',
		'PY' => 'PYG',
		'PE' => 'PEI',
		'PH' => 'PHP',
		'PN' => 'NZD',
		'PL' => 'PLN',
		'PT' => 'EUR',
		'PR' => 'USD',
		'QA' => 'QAR',
		'RE' => 'EUR',
		'RO' => 'ROL',
		'RU' => 'RUB',
		'RW' => 'RWF',
		'SH' => 'SHP',
		'KN' => 'XCD',
		'LC' => 'XCD',
		'PM' => 'EUR',
		'VC' => 'XCD',
		'WS' => 'WST',
		'SM' => 'EUR',
		'ST' => 'STD',
		'SA' => 'SAR',
		'SN' => 'XOF',
		'SC' => 'SCR',
		'SL' => 'SLL',
		'SG' => 'SGD',
		'SK' => 'SKK',
		'SI' => 'SIT',
		'SB' => 'SBD',
		'SO' => 'SOS',
		'ZA' => 'ZAR',
		'GS' => 'GBP',
		'ES' => 'EUR',
		'LK' => 'LKR',
		'SD' => 'SDG',
		'SR' => 'SRG',
		'SJ' => 'NOK',
		'SZ' => 'SZL',
		'SE' => 'SEK',
		'CH' => 'CHF',
		'SY' => 'SYP',
		'TW' => 'TWD',
		'TJ' => 'TJR',
		'TZ' => 'TZS',
		'TH' => 'THB',
		'TL' => 'TPE',
		'TG' => 'XAF',
		'TK' => 'NZD',
		'TO' => 'TOP',
		'TT' => 'TTD',
		'TN' => 'TND',
		'TR' => 'TRL',
		'TM' => 'TMM',
		'TC' => 'USD',
		'TV' => 'AUD',
		'UG' => 'UGS',
		'UA' => 'UAH',
		'AE' => 'AED',
		'GB' => 'GBP',
		'US' => 'USD',
		'UM' => 'USD',
		'UY' => 'UYU',
		'UZ' => 'UZS',
		'VU' => 'VUV',
		'VE' => 'VEB',
		'VN' => 'VND',
		'VG' => 'USD',
		'VI' => 'USD',
		'WF' => 'XPF',
		'EH' => 'MAD',
		'YE' => 'YER',
		'ZM' => 'ZMK',
		'ZW' => 'ZWD',
	);
