# Opti World Currency #

### About ###

* Version 1.6
* Opti World Currency is a WordPress plugin based on World Currency featuring a few optimisations and removing the ip2c dependency. This plugin will continue to use the same shortcodes used by World Currency so that you won't need to change your posts when switching plugins. Unexpected behaviour may (and probably will) occur when this plugin and World Currency are both active! The defaults have been set for my requirements. It uses a cron job to periodically cache currency conversion rates from Yahoo to minimise load times.
* Big up to the original developers mentioned [here](https://wordpress.org/plugins/geoip-detect/).

### How do I get set up? ###

* Download the repo as a zip file.
* Upload the zipped plugin to your WordPress install.
* Deactivate World Currency if you haven't already.
* Activate and set up Opti World Currency.
* Enjoy.

### Credits ###

* Modified by Simon Urban

### Changelog ###

.: 1.6 :.

* Added PHP7 compatibility

.: 1.5 :.

* Added TinyMCE support by merging [Opti World Currency TinyMCE](https://bitbucket.org/Psycic101/opti-world-currency-tinymce) with this project.
* Changed default output format (different country)

.: 1.4 :.

* The generated shortcode spans now show "Loading rates..." until the converted rate is obtained.

.: 1.3 :.

* Fixed the output formats not saving (always resetting to defaults) in the admin page.

.: 1.2 :.

* Shortcodes now work when using WP Fastest Cache's Minify HTML Plus feature. (WPFC is now fully supported).
* Fixed minor usability issues.

.: 1.1 :.

* Changed visitor location detection from PHP code using GeoIP Detection (WordPress plugin) to a jQuery AJAX implementation using Free GeoIP.

.: 1.0 :.

* Initial codebase upload with major changes I required to warrant the change from WorldCurrency to Opti World Currency.
