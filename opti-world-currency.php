<?php
/*
Plugin Name: Opti World Currency
Plugin URI: https://www.eastcapetours.com/ect/opti-world-currency/
Description: This is a variant of WorldCurrency where conversion rates are (better) cached locally to reduce queries to Yahoo servers and visitor location queries now bypass caches (tested on Cloudflare and WP Fastest Cache).
Version: 1.6
Author: Simon Urban
Author URI: https://www.eastcapetours.com/ect/simon-urban/
License: GPLv2

Opti World Currency is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Opti World Currency is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Opti World Currency. If not, see https://www.gnu.org/licenses/gpl-2.0.html
*/

// Loads currency infos
require_once 'currencies.inc.php';

// Retrieve current saved options from Wordpress
$dt_owc_options = get_option('dt_owc_options');

// Register HOOKS
register_activation_hook(__FILE__, 'dt_owc_createOptions');
add_action('admin_menu', 'dt_owc_adminPage');
add_action('wp_head', 'dt_owc_head');
add_action('publish_page', 'dt_owc_publish');
add_action('publish_post', 'dt_owc_publish');
add_shortcode('worldcurrency', 'dt_owc_shortcode');
add_shortcode('worldcurrencybox', 'dt_owc_shortcode_box');
add_filter('the_content', 'dt_owc_content', $dt_owc_options['plugin_priority']);

add_action( 'wp_ajax_nopriv_worldcurrency', 'dt_owc_ajaxGetExchangeRate' );
add_action( 'wp_ajax_worldcurrency', 'dt_owc_ajaxGetExchangeRate' );
add_action( 'wp_ajax_nopriv_worldcurrencybox', 'dt_owc_ajaxGetCurrencySelectionBox' );
add_action( 'wp_ajax_worldcurrencybox', 'dt_owc_ajaxGetCurrencySelectionBox' );

// Register Widget
require_once 'opti-world-currency.widget.php';

// Create options on Activation
function dt_owc_createOptions($force = false) {

	$dt_owc_options = get_option('$dt_owc_options');

	if ($force || !isset($dt_owc_options['plugin_link']))			$dt_owc_options['plugin_link'] = 'false';
	if ($force || !isset($dt_owc_options['yahoo_link']))			$dt_owc_options['yahoo_link'] = 'false';
	if ($force || !isset($dt_owc_options['cache_rates']))			$dt_owc_options['cache_rates'] = null;
	if ($force || !isset($dt_owc_options['cache_time']))			$dt_owc_options['cache_time'] = 0;
	if ($force || !isset($dt_owc_options['historic_rates']))		$dt_owc_options['historic_rates'] = 'false';
	if ($force || !isset($dt_owc_options['hide_if_same']))			$dt_owc_options['hide_if_same'] = 'false';
	if ($force || !isset($dt_owc_options['output_format_diff']))	$dt_owc_options['output_format_diff'] = '%from_symbol% %from_value% (%to_code% %to_symbol% %to_value%)';
	if ($force || !isset($dt_owc_options['output_format_same']))	$dt_owc_options['output_format_same'] = '%from_symbol% %from_value%';
	if ($force || !isset($dt_owc_options['thousands_separator']))	$dt_owc_options['thousands_separator'] = ',';
	if ($force || !isset($dt_owc_options['decimal_separator']))		$dt_owc_options['decimal_separator'] = '.';
	if ($force || !isset($dt_owc_options['bottom_select']))			$dt_owc_options['bottom_select'] = 'false';
	if ($force || !isset($dt_owc_options['include_jquery']))		$dt_owc_options['include_jquery'] = 'false';
	if ($force || !isset($dt_owc_options['include_always']))		$dt_owc_options['include_always'] = 'true';
	if ($force || !isset($dt_owc_options['jquery_no_conflict']))	$dt_owc_options['jquery_no_conflict'] = 'true';
	if ($force || !isset($dt_owc_options['ajax_over_ssl']))			$dt_owc_options['ajax_over_ssl'] = 'false';
	if ($force || !isset($dt_owc_options['plugin_priority']))		$dt_owc_options['plugin_priority'] = 5;
	if ($force || !isset($dt_owc_options['additional_css']))		$dt_owc_options['additional_css'] = <<<EOT
.worldcurrency {
	color: #888;
}
.worldcurrency_selection_box {
	margin: 10px 0px 10px 0px;
	border: 1px dashed #CCC;
	padding: 5px 5px 3px 5px;
	background-color: #F2F2F2;
	line-height: 18px;
}
.worldcurrency_selection_box .credits {
	font-size: 11px;
}
EOT;

	update_option('dt_owc_options', $dt_owc_options);
}

/////// CRON CODE ////////
// create a scheduled event (if it does not exist already) to ensure rates are cached
function cacherates_cronstarter_activation() {
	if( !wp_next_scheduled( 'cacheratesfromyahoo' ) ) {
	   wp_schedule_event( time(), 'twicedaily', 'cacheratesfromyahoo' );
	}
}
// and make sure it's called whenever WordPress loads
add_action('wp', 'cacherates_cronstarter_activation');

// unschedule event upon plugin deactivation
function cacherates_cronstarter_deactivate() {
	// find out when the last event was scheduled
	$timestamp = wp_next_scheduled ('cacheratesfromyahoo');
	// unschedule previous event if any
	wp_unschedule_event ($timestamp, 'cacheratesfromyahoo');
}
register_deactivation_hook (__FILE__, 'cacherates_cronstarter_deactivate');

// fetch current rates and cache them
function cache_current_rates() {
	// Get rates from Yahoo!Finance and store them
	$dt_owc_options['cache_rates'] = $serializedQuotes = $YahooFinance->getSerializedQuotes();
	$dt_owc_options['cache_time'] = time();
	update_option('dt_owc_options', $dt_owc_options);
}

// hook that function onto our scheduled event:
add_action ('cacheratesfromyahoo', 'cache_current_rates');
//////////////////////////

// Create the options page
function dt_owc_adminPage() {
	require_once('opti-world-currency-admin.php');
}

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'optiworldcurrency_add_action_links' );
function optiworldcurrency_add_action_links ( $links ) {
	$mylinks = array(
	'<a href="' . admin_url( 'options-general.php?page=opti-world-currency-admin.php' ) . '">Settings</a>',
	);
	return array_merge( $links, $mylinks );
}

// Add page scripts
function dt_owc_head() {
	global $post;

	$dt_owc_options = get_option('dt_owc_options');

	$jQuerySymbol = $dt_owc_options['jquery_no_conflict'] == 'true' ? 'jQuery' : '$';

	// Include the script only if necessary
	if ($dt_owc_options['include_always'] == 'true' || get_post_meta($post->ID, 'owc_force', true) == 1 || strpos($post->post_content, 'worldcurrency') !== false) {

		echo "\n<!-- DT OptiWorldCurrency Code -->\n";

		// Include jQuery if needed
		if ($dt_owc_options['include_jquery'] == 'true')
			echo "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>\n";

		if ($dt_owc_options['ajax_over_ssl'] == 'true') {
			$ajax_url = wp_nonce_url(str_replace('http:','https:', admin_url('admin-ajax.php')), 'worldcurrency_safe');
		} else {
			$ajax_url = wp_nonce_url(str_replace('https:','http:', admin_url('admin-ajax.php')), 'worldcurrency_safe');
		}

		?>
		<script type="text/javascript">
			// Global location variable
			var countryCode;

			function worldCurInit() {
			<?php if ($dt_owc_options['jquery_no_conflict'] == 'true'): ?>
				jQuery.noConflict();
			<?php endif; ?>

			dt_optiWorldCurrency_update = function() {
				// For each worldcurrency <span> get the value
				<?php echo $jQuerySymbol; ?>('.worldcurrency').each(function() {
					var theSpan = <?php echo $jQuerySymbol; ?>(this);
					theSpan.html('Loading rates...');
					<?php echo $jQuerySymbol; ?>.ajax({
						url: '<?php echo $ajax_url; ?>',
						dataType: 'html',
						type: 'GET',
						data: {'action':'worldcurrency', 'to':theSpan.attr('target') ? theSpan.attr('target') : countryCode, 'from':theSpan.attr('curr'), 'value':theSpan.attr('value'), 'postId':theSpan.attr('postId'), 'historic':theSpan.attr('historic') ? theSpan.attr('historic') : '<?php echo $dt_owc_options['historic_rates']; ?>'},
						success: function(html, textStatus) {
							theSpan.html(html);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							window.console&&console.log(textStatus + ': ', + errorThrown);
						}
					});
				});
				// For each Currency selection box, chose the right value
				<?php echo $jQuerySymbol; ?>('.worldcurrency_select').val(countryCode);
			}

			// When the page is loaded
			<?php echo $jQuerySymbol; ?>(document).ready(function() {
				// Register already rendered currency selection boxes
				<?php echo $jQuerySymbol; ?>('.worldcurrency_select').change(function() {
					countryCode = <?php echo $jQuerySymbol; ?>(this).attr('value');
					<?php echo $jQuerySymbol; ?>(this).trigger('blur');
					dt_optiWorldCurrency_update();
				});

				// Render currency selection boxes
				<?php echo $jQuerySymbol; ?>('.worldcurrency_selection_box_placeholder').each(function() {
					var theDiv = <?php echo $jQuerySymbol; ?>(this);
					<?php echo $jQuerySymbol; ?>.ajax({
						url: '<?php echo $ajax_url; ?>',
						dataType: 'html',
						type: 'GET',
						data: {'action':'worldcurrencybox'},
						success: function(html, textStatus) {
							theDiv.html(html);

							theSelect = theDiv.children('.worldcurrency_selection_box').children('.worldcurrency_select');
							// Set the current currency
							theSelect.val(countryCode);
							countryCode = <?php echo $jQuerySymbol; ?>(this).attr('value');
							// On change update currency
							theSelect.change(function() {
								<?php echo $jQuerySymbol; ?>(this).trigger('blur');
								dt_optiWorldCurrency_update();
							});
						},
						error: function(jqXHR, textStatus, errorThrown) {
							window.console&&console.log(textStatus + ': ', + errorThrown);
						}
					});
				});
				dt_optiWorldCurrency_update();
			});
			}

			// get user location using jQuery AJAX
			// alternate methods to obtain location: http://stackoverflow.com/a/35123097
			function getUserLocation() {
				<?php echo $jQuerySymbol; ?>.ajax({
					url: '//freegeoip.net/json/',
					type: 'POST',
					dataType: 'jsonp',
					success: function(location) {
						countryCode = location.country_code;
						var locationList = {};
						locationList.AF = 'AFA';
						locationList.AL = 'ALL';
						locationList.DZ = 'DZD';
						locationList.AS = 'USD';
						locationList.AD = 'EUR';
						locationList.AO = 'AON';
						locationList.AI = 'XCD';
						locationList.AQ = 'NOK';
						locationList.AG = 'XCD';
						locationList.AR = 'ARS';
						locationList.AM = 'AMD';
						locationList.AW = 'AWG';
						locationList.AU = 'AUD';
						locationList.AT = 'EUR';
						locationList.AZ = 'AZM';
						locationList.BS = 'BSD';
						locationList.BH = 'BHD';
						locationList.BD = 'BDT';
						locationList.BB = 'BBD';
						locationList.BY = 'BYR';
						locationList.BE = 'EUR';
						locationList.BZ = 'BZD';
						locationList.BJ = 'XAF';
						locationList.BM = 'BMD';
						locationList.BT = 'BTN';
						locationList.BO = 'BOB';
						locationList.BA = 'BAM';
						locationList.BW = 'BWP';
						locationList.BV = 'NOK';
						locationList.BR = 'BRR';
						locationList.IO = 'GBP';
						locationList.BN = 'BND';
						locationList.BG = 'BGN';
						locationList.BF = 'XAF';
						locationList.BI = 'BIF';
						locationList.KH = 'KHR';
						locationList.CM = 'XAF';
						locationList.CA = 'CAD';
						locationList.CV = 'CVE';
						locationList.KY = 'KYD';
						locationList.CF = 'XAF';
						locationList.TD = 'XAF';
						locationList.CL = 'CLF';
						locationList.CN = 'CNY';
						locationList.CX = 'AUD';
						locationList.CC = 'AUD';
						locationList.CO = 'COP';
						locationList.KM = 'KMF';
						locationList.CG = 'XAF';
						locationList.CK = 'NZD';
						locationList.CR = 'CRC';
						locationList.CI = 'XAF';
						locationList.HR = 'HRK';
						locationList.CU = 'CUP';
						locationList.CY = 'CYP';
						locationList.CZ = 'CZK';
						locationList.DK = 'DKK';
						locationList.DJ = 'DJF';
						locationList.DM = 'XCD';
						locationList.DO = 'DOP';
						locationList.EC = 'USD';
						locationList.EG = 'EGP';
						locationList.SV = 'USD';
						locationList.GQ = 'XAF';
						locationList.ER = 'ERN';
						locationList.EE = 'EEK';
						locationList.ET = 'ETB';
						locationList.FK = 'FKP';
						locationList.FO = 'DKK';
						locationList.FJ = 'FJD';
						locationList.FI = 'EUR';
						locationList.FR = 'EUR';
						locationList.GF = 'EUR';
						locationList.PF = 'XPF';
						locationList.TF = 'EUR';
						locationList.GA = 'XAF';
						locationList.GM = 'GMD';
						locationList.GE = 'GEL';
						locationList.DE = 'EUR';
						locationList.GH = 'GHC';
						locationList.GI = 'GIP';
						locationList.GR = 'EUR';
						locationList.GL = 'DKK';
						locationList.GD = 'XCD';
						locationList.GP = 'EUR';
						locationList.GU = 'USD';
						locationList.GT = 'GTQ';
						locationList.GG = 'GBP';
						locationList.GN = 'GNS';
						locationList.GW = 'GWP';
						locationList.GY = 'GYD';
						locationList.HT = 'HTG';
						locationList.HM = 'AUD';
						locationList.VA = 'EUR';
						locationList.HN = 'HNL';
						locationList.HK = 'HKD';
						locationList.HU = 'HUF';
						locationList.IS = 'ISK';
						locationList.IN = 'INR';
						locationList.ID = 'IDR';
						locationList.IR = 'IRR';
						locationList.IQ = 'IQD';
						locationList.IE = 'EUR';
						locationList.IM = 'GBP';
						locationList.IL = 'ILS';
						locationList.IT = 'EUR';
						locationList.JM = 'JMD';
						locationList.JP = 'JPY';
						locationList.JE = 'GBP';
						locationList.JO = 'JOD';
						locationList.KZ = 'KZT';
						locationList.KE = 'KES';
						locationList.KI = 'AUD';
						locationList.KR = 'KRW';
						locationList.KW = 'KWD';
						locationList.KG = 'KGS';
						locationList.LA = 'LAK';
						locationList.LV = 'LVL';
						locationList.LB = 'LBP';
						locationList.LS = 'LSL';
						locationList.LR = 'LRD';
						locationList.LY = 'LYD';
						locationList.LI = 'CHF';
						locationList.LT = 'LTL';
						locationList.LU = 'EUR';
						locationList.MO = 'MOP';
						locationList.MK = 'MKD';
						locationList.MG = 'MGF';
						locationList.MW = 'MWK';
						locationList.MY = 'MYR';
						locationList.MV = 'MVR';
						locationList.ML = 'XAF';
						locationList.MT = 'MTL';
						locationList.MH = 'USD';
						locationList.MQ = 'EUR';
						locationList.MR = 'MRO';
						locationList.MU = 'MUR';
						locationList.YT = 'EUR';
						locationList.MX = 'MXN';
						locationList.MC = 'EUR';
						locationList.MN = 'MNT';
						locationList.ME = 'EUR';
						locationList.MS = 'XCD';
						locationList.MA = 'MAD';
						locationList.MZ = 'MZM';
						locationList.MM = 'MMK';
						locationList.NA = 'NAD';
						locationList.NR = 'AUD';
						locationList.NP = 'NPR';
						locationList.NL = 'EUR';
						locationList.AN = 'ANG';
						locationList.NC = 'XPF';
						locationList.NZ = 'NZD';
						locationList.NI = 'NIC';
						locationList.NE = 'XOF';
						locationList.NG = 'NGN';
						locationList.NU = 'NZD';
						locationList.NF = 'AUD';
						locationList.MP = 'USD';
						locationList.NO = 'NOK';
						locationList.OM = 'OMR';
						locationList.PK = 'PKR';
						locationList.PW = 'USD';
						locationList.PA = 'PAB';
						locationList.PG = 'PGK';
						locationList.PY = 'PYG';
						locationList.PE = 'PEI';
						locationList.PH = 'PHP';
						locationList.PN = 'NZD';
						locationList.PL = 'PLN';
						locationList.PT = 'EUR';
						locationList.PR = 'USD';
						locationList.QA = 'QAR';
						locationList.RE = 'EUR';
						locationList.RO = 'ROL';
						locationList.RU = 'RUB';
						locationList.RW = 'RWF';
						locationList.SH = 'SHP';
						locationList.KN = 'XCD';
						locationList.LC = 'XCD';
						locationList.PM = 'EUR';
						locationList.VC = 'XCD';
						locationList.WS = 'WST';
						locationList.SM = 'EUR';
						locationList.ST = 'STD';
						locationList.SA = 'SAR';
						locationList.SN = 'XOF';
						locationList.SC = 'SCR';
						locationList.SL = 'SLL';
						locationList.SG = 'SGD';
						locationList.SK = 'SKK';
						locationList.SI = 'SIT';
						locationList.SB = 'SBD';
						locationList.SO = 'SOS';
						locationList.ZA = 'ZAR';
						locationList.GS = 'GBP';
						locationList.ES = 'EUR';
						locationList.LK = 'LKR';
						locationList.SD = 'SDG';
						locationList.SR = 'SRG';
						locationList.SJ = 'NOK';
						locationList.SZ = 'SZL';
						locationList.SE = 'SEK';
						locationList.CH = 'CHF';
						locationList.SY = 'SYP';
						locationList.TW = 'TWD';
						locationList.TJ = 'TJR';
						locationList.TZ = 'TZS';
						locationList.TH = 'THB';
						locationList.TL = 'TPE';
						locationList.TG = 'XAF';
						locationList.TK = 'NZD';
						locationList.TO = 'TOP';
						locationList.TT = 'TTD';
						locationList.TN = 'TND';
						locationList.TR = 'TRL';
						locationList.TM = 'TMM';
						locationList.TC = 'USD';
						locationList.TV = 'AUD';
						locationList.UG = 'UGS';
						locationList.UA = 'UAH';
						locationList.AE = 'AED';
						locationList.GB = 'GBP';
						locationList.US = 'USD';
						locationList.UM = 'USD';
						locationList.UY = 'UYU';
						locationList.UZ = 'UZS';
						locationList.VU = 'VUV';
						locationList.VE = 'VEB';
						locationList.VN = 'VND';
						locationList.VG = 'USD';
						locationList.VI = 'USD';
						locationList.WF = 'XPF';
						locationList.EH = 'MAD';
						locationList.YE = 'YER';
						locationList.ZM = 'ZMK';
						locationList.ZW = 'ZWD';
						countryCode = locationList[countryCode];
					},
					error: function(jqXHR, textStatus, errorThrown) {
						countryCode = 'GBP';
						window.console&&console.log(textStatus + ': ', + errorThrown);
					}
				});
			}

			function runWCWhenLocationIsLoaded() {
				if (countryCode){
					// process page as usual
					worldCurInit();
				} else {
					setTimeout(runWCWhenLocationIsLoaded, 5);
				}
			}

			function runWCWhenJQueryIsLoaded() {
				if (window.jQuery){
					// run jQuery method to obtain country and write cookie
					getUserLocation();
					runWCWhenLocationIsLoaded();
				} else {
					setTimeout(runWCWhenJQueryIsLoaded, 5);
				}
			}
			runWCWhenJQueryIsLoaded();
		</script>
		<style type="text/css">
		<?php echo $dt_owc_options['additional_css']; ?>
		</style>
		<!-- End of Opti World Currency DT Extension Code -->
		<?php
	}
}

/**
 * Handler for [worldcurrency] shortcode
 *
 * USAGE:
 *
 * 		[worldcurrency cur="EUR" value="25"]
 * 			in united states will show:
 * 			30 USD
 *
 * Parameters:
 *
 * 		curr="***" 				-> the name of the value currency
 * 		target="***" 			-> the name of the target currency (if you want to force it)
 * 		value="***"				-> the value used for exchange
 * 		historic="true|false"	-> (optional) override main plugin setting
 *
 * @param array $attr
 */
function dt_owc_shortcode($attr) {
	global $post;

	if (!isset($attr['curr']) || !isset($attr['value']))
		return '[worldcurrency error: curr="" and value="" parameters needed]';

	$out = '<span class="worldcurrency" postId="'.$post->ID.'" curr="'.$attr['curr'].'" value="'.$attr['value'].'" ';

	if (isset($attr['historic']))	$out .= 'historic="'.$attr['historic'].'" ';
	if (isset($attr['target']))		$out .= 'target="'.$attr['target'].'" ';

	$out .= '></span>';

	return $out;
}

/**
 * Handler for [worldcurrencybox] shortcode that shows the currency selection box
 */
function dt_owc_shortcode_box() {
	return dt_owc_getCurrencySelectionBox();
}

/**
 * On publishing/updating post/page saves the current currency conversion rates for future historic uses
 */
function dt_owc_publish() {
	global $post;

	// Check if there are rates attached to the post or else saves them
	if (!($serializedQuotes = get_post_meta($post->ID, 'owc_rates', true))) {

		// Include our Yahoo!Finance class
		require_once 'yahoofinance.class.php';
		$YahooFinance = new yahoofinance();

		update_post_meta($post->ID, 'owc_rates', $serializedQuotes = $YahooFinance->getSerializedQuotes());
		update_post_meta($post->ID, 'owc_rates_date', time());

	}
}

/**
 * Adds a currecy selection box at the end of the page/post if needed
 * @param string $theContent
 */
function dt_owc_content($theContent) {
	global $dt_owc_options;
	if ($dt_owc_options['bottom_select'] == 'true' && strpos($theContent, 'worldcurrency') !== false)
		$theContent .= dt_owc_getCurrencySelectionBox();
	return $theContent;
}

/**
 * Generate and returns the HTML for the currency selection box
 */
function dt_owc_getCurrencySelectionBox() {
	$out = '';

	// Include our Yahoo!Finance class
		require_once 'currencies.inc.php';
		global $dt_owc_currencylist;

	// Retrieve current WC saved options from Wordpress
		$dt_owc_options = get_option('dt_owc_options');

	$out .= '<div class="worldcurrency_selection_box">';

		// Renders the select box
		$out .= '<div style="float:left;margin-right:4px;">Show currencies in:</div>';
		$out .= '<select class="worldcurrency_select">';
		foreach ($dt_owc_currencylist as $currencyCode => $currencyInfo) {
			if (in_array($currencyCode, array('---'))) continue;
			$out .= '<option value="'.$currencyCode.'">'.$currencyInfo['name'].'</option>';
		}
		$out .= '</select>';

		// Renders the credits
		if ($dt_owc_options['plugin_link'] == 'true' || $dt_owc_options['yahoo_link'] == 'true') $out .= '<div class="credits">Powered by';
		if ($dt_owc_options['plugin_link'] == 'true') $out .= ' the <a href="http://www.cometicucinoilweb.it/blog/en/worldcurrency-plugin-for-wordpress/" target="_blank" title="Opti World Currency plugin for Wordpress">Opti World Currency</a> plugin.';
		if ($dt_owc_options['yahoo_link'] == 'true') $out .= ' <a href="http://finance.yahoo.com" title="Visit Yahoo! Finance" target="_blank">Yahoo! Finance</a> for the rates.';
		if ($dt_owc_options['plugin_link'] == 'true' || $dt_owc_options['yahoo_link'] == 'true') $out .= '</div>';

	$out .= '</div>'."\n";

	return $out;
}

function dt_owc_ajaxGetCurrencySelectionBox() {
	// Make sure there's nothing bad in the URL
	foreach ($_GET as $key => $value)
		$_GET[$key] = htmlentities(stripslashes($value));

	// Don't proceed if we don't have enough info or if the nonce fails :: Ignoring nonce
	// if (!check_admin_referer('worldcurrency_safe'))
	// 	die();

	echo dt_owc_getCurrencySelectionBox();

	exit;
}

function dt_owc_ajaxGetExchangeRate() {
	// Make sure there's nothing bad in the URL
	foreach ($_GET as $key => $value)
		$_GET[$key] = htmlentities(stripslashes($value));

	// Don't proceed if we don't have enough info or if the nonce fails
	// if (!isset($_GET['value']) || !isset($_GET['historic']) || !isset($_GET['from']) || !isset($_GET['to']) || !check_admin_referer('worldcurrency_safe'))
	// 	exit;

	// Don't proceed if we don't have enough info :: Ignoring nonce
	if (!isset($_GET['value']) || !isset($_GET['historic']) || !isset($_GET['from']) || !isset($_GET['to']))
		exit;

	// Include our Yahoo!Finance class
	require_once 'yahoofinance.class.php';
	global $dt_owc_currencylist;
	$YahooFinance = new yahoofinance();

	// Retrieve current OWC saved options from Wordpress
	$dt_owc_options = get_option('dt_owc_options');

	// Check if we need only historic rates
	if ($_GET['historic'] == 'true' && isset($_GET['postId'])) {
		// Check if there are rates attached to the post or else saves them
		if (!($serializedQuotes = get_post_meta($_GET['postId'], 'owc_rates', true))) {
			update_post_meta($_GET['postId'], 'owc_rates', $serializedQuotes = $YahooFinance->getSerializedQuotes());
			update_post_meta($_GET['postId'], 'owc_rates_date', time());
		}
	} else {
		if ($dt_owc_options['cache_rates'] == null) {
			// get latest rates from Yahoo and cache them
			$dt_owc_options['cache_rates'] = $serializedQuotes = $YahooFinance->getSerializedQuotes();
			$dt_owc_options['cache_time'] = time();
			update_option('dt_owc_options', $dt_owc_options);
		} else {
			// Fetch Rates from options if they are fresh
			$serializedQuotes = $dt_owc_options['cache_rates'];
		}
	}

	// Get the separators format
	$thousands_separator = isset($dt_owc_options['thousands_separator']) ? $dt_owc_options['thousands_separator'] : '.';
	$decimals_separator = isset($dt_owc_options['decimal_separator']) ? $dt_owc_options['decimal_separator'] : ',';

	// Load the quotes obtained
	$YahooFinance->loadSerializedQuotes($serializedQuotes);

	// Fetch the desired exchange rate and prepare all the parameters
	if ($_GET['from'] == $_GET['to']) {
		$exchange_rate = 1;
	} else {
		$exchange_rate	= $YahooFinance->getExchangeRate($_GET['from'], $_GET['to']);
	}

	$from_code		= $_GET['from'];
	$from_value		= $_GET['value'];
	$from_name		= $dt_owc_currencylist[$from_code]['name'];
	$from_symbol	= $dt_owc_currencylist[$from_code]['symbol'];

	$to_code		= $_GET['to'];
	$to_value		= $from_value * $exchange_rate;
	$to_name		= $dt_owc_currencylist[$to_code]['name'];
	$to_symbol		= $dt_owc_currencylist[$to_code]['symbol'];

	// Round the numbers
	$exchange_rate = number_format($exchange_rate,2,',','.');
	$from_value = $from_value > 100 ? number_format($from_value,0,$decimals_separator,$thousands_separator) : number_format($from_value, 2,$decimals_separator,$thousands_separator);
	$to_value = $to_value > 100 ? number_format($to_value,0,$decimals_separator,$thousands_separator) : number_format($to_value, 2,$decimals_separator,$thousands_separator);

	// Show only original value if conversion is to the same currency
	if ($from_code == $to_code) {
		echo str_replace(array('%exchange_rate%','%from_code%','%from_value%','%from_name%','%from_symbol%','%to_code%','%to_value%','%to_name%','%to_symbol%'), array($exchange_rate,$from_code,$from_value,$from_name,$from_symbol,$to_code,$to_value,$to_name,$to_symbol), $dt_owc_options['output_format_same']);
		// $dt_wc_options['output_format'] = '%from_symbol% %from_value%';
	} else {
		echo str_replace(array('%exchange_rate%','%from_code%','%from_value%','%from_name%','%from_symbol%','%to_code%','%to_value%','%to_name%','%to_symbol%'), array($exchange_rate,$from_code,$from_value,$from_name,$from_symbol,$to_code,$to_value,$to_name,$to_symbol), $dt_owc_options['output_format_diff']);
		// $dt_wc_options['output_format'] = '%from_symbol% %from_value% (%to_symbol% %to_value%)';
	}

	exit;
}

class OWC_TinyMCE_Class {
	function __construct() {
    if (is_admin()) {
      add_action('init', array($this, 'setup_tinymce_plugin'));
    }
  }

  // Check if the current user can edit Posts or Pages and is using the Visual Editor
  function setup_tinymce_plugin() {
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
      return;
    }
    if (get_user_option('rich_editing') !== 'true') {
      return;
    }

    add_filter('mce_external_plugins', array(&$this, 'add_tinymce_plugin'));
    add_filter('mce_buttons', array(&$this, 'add_tinymce_toolbar_button'));
  }

  // Loads the TinyMCE compatible JS file
  function add_tinymce_plugin($plugin_array) {
    $plugin_array['worldcurrency'] = plugin_dir_url(__FILE__) . 'opti-world-currency-tinymce.js';
    return $plugin_array;
  }

  // Add button to TinyMCE toolbar
  function add_tinymce_toolbar_button($buttons) {
    array_push($buttons, '|', 'worldcurrency');
    return $buttons;
  }
}

$owc_tinymce_class = new OWC_TinyMCE_Class;
