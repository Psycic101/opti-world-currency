(function($) {
  tinymce.create('tinymce.plugins.worldcurrency', {
    init: function(editor, url) {
      editor.addButton('worldcurrency', {
        title: 'Insert Opti World Currency',
            image: url + '/icon.png',
            cmd: 'worldcurrency'
      });

      editor.addCommand('worldcurrency', function() {
        editor.windowManager.open(
          {
            title: 'Opti World Currency Shortcode',
            file: url + '/opti-world-currency-tinymce-dialog.html',
            width: 300,
            height: 200,
            inline: 1
          },

          {
            editor: editor,
            jquery: $,
          }
        );
      });
    }
  });

  tinymce.PluginManager.add('worldcurrency', tinymce.plugins.worldcurrency);
  // tinymce.PluginManager.add('worldcurrency', function(editor, url) {
  //   editor.addButton('worldcurrency', {
  //     title: 'Insert Opti World Currency',
  //     image: url + '/icon.png',
  //     cmd: 'worldcurrency',
  //   });
  //   editor.addCommand('worldcurrency', function() {
  //     // Check that no text is selected
  //     var text = editor.selection.getContent({
  //       'format': 'html'
  //     });
  //     if (text.length !== 0) {
  //       alert('Please don\'t select text in the editor.');
  //       return;
  //     }
  //
  //     // Ask the user to enter the value
  //     var result = prompt('Enter amount');
  //     if (!result) {
  //       return;
  //     } else if (result.length === 0) {
  //       return;
  //     }
  //
  //     // [worldcurrency curr="GBP" value="25"]
  //     editor.execCommand('mceReplaceContent', false, '[worldcurrency curr="GBP" value="' + result + '"]');
  //   });
  // });
})(jQuery);
